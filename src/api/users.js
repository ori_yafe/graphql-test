import axios from "axios";
import { restApi } from "../config/config.js";

// Add Authorization header
axios.interceptors.request.use(
  function (config) {
    config.headers = {
      ...config.headers,
      authorization: restApi.authorization,
    };

    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default {
  getAll: () => axios.get(`${restApi.host}/users`).then((res) => res.data),
  getById: (userId) =>
    axios.get(`${restApi.host}/users/${userId}`).then((res) => res.data),
  add: (userToAdd) =>
    axios
      .post(`${restApi.host}/users`, { user: userToAdd })
      .then((res) => res.data),
};
