import { config as configFunc } from "dotenv";

configFunc();

const env = process.env.NODE_ENV ?? "development";

const development = {
  restApi: {
    host: process.env.DEV_REST_HOST,
    authorization: process.env.DEV_AUTH_TOKEN,
  },
};

const config = {
  development,
};

export default config[env];

export const restApi = config[env].restApi;
