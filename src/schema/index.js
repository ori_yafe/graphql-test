import { GraphQLSchema } from "graphql";
import mutation from "./mutation.js";
import query from "./query.js";

export default new GraphQLSchema({
  mutation,
  query,
});
