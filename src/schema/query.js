import { GraphQLObjectType } from "graphql";
import user from "./queries/user.js";
import users from "./queries/users.js";

const query = new GraphQLObjectType({
  name: "Query",
  fields: () => ({
    user,
    users,
  }),
});

export default query;
