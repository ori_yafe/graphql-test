import { GraphQLID, GraphQLNonNull } from "graphql";
import userType from "../types/user.js";
import userApi from "../../api/users.js";

const user = {
  type: userType,
  args: {
    id: {
      type: GraphQLNonNull(GraphQLID),
    },
  },
  resolve: (_, { id }) => {
    return userApi.getById(id);
  },
};

export default user;
