import { GraphQLNonNull, GraphQLList } from "graphql";
import { default as userType } from "../types/user.js";
import userApi from "../../api/users.js";

const users = {
  type: GraphQLNonNull(GraphQLList(GraphQLNonNull(userType))),
  resolve: () => {
    return userApi.getAll();
  },
};

export default users;
