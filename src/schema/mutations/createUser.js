import { GraphQLNonNull } from "graphql";
import userApi from "../../api/users.js";
import userType from "../types/user.js";
import newUser from "../types/inputs/newUser.js";

const createUser = {
  type: userType,
  args: {
    input: {
      type: GraphQLNonNull(newUser),
    },
  },
  resolve: (_, { input }) => {
    return userApi.add(input);
  },
};

export default createUser;
