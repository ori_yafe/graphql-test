import { GraphQLInputObjectType, GraphQLNonNull, GraphQLString } from "graphql";

const newUser = new GraphQLInputObjectType({
  name: "NewUser",
  fields: {
    email: {
      type: GraphQLNonNull(GraphQLString),
    },
    username: {
      type: GraphQLNonNull(GraphQLString),
    },
    password: {
      type: GraphQLNonNull(GraphQLString),
    },
  },
});

export default newUser;
