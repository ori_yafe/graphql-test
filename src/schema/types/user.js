import {
  GraphQLID,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean,
} from "graphql";

const user = new GraphQLObjectType({
  name: "User",
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLID),
      description: "The id of the user in the db",
      resolve: (obj) => {
        return obj.id;
      },
    },
    email: {
      type: GraphQLNonNull(GraphQLString),
      description: "The email of the user",
      resolve: (obj) => {
        return obj.email;
      },
    },
    username: {
      type: GraphQLNonNull(GraphQLString),
      description: "The username of the user",
      resolve: (obj) => {
        return obj.username;
      },
    },
    isAdmin: {
      type: GraphQLNonNull(GraphQLBoolean),
      description: "The username of the isAdmin",
      resolve: (obj) => {
        return obj.isAdmin;
      },
    },
  }),
});

export default user;
